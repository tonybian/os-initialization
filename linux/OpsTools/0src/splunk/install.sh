#!/bin/bash
#Desc: Install splunkforwarder in OneKey.
#Date: 2016-05-04
#Author: tongheb@tujia.com

DIRNAME=$(
    cd $(dirname $0)
    pwd
)
BASE_DIRNAME=$(dirname "$DIRNAME")

. $BASE_DIRNAME/install.conf

#安装用户检测
ROOT_UID=0

if [ "$UID" -ne "$ROOT_UID" ]; then
    echo "[Permission denied.Please run this script using 'root' user.Press any key to exit]" && read -n 1
fi

#设置splunk服务器地址
if [ -z $1 ]; then
    Server="$SPLUNK_SERVER"
else
    Server=$1
fi

#检测是否已安装
# netstat -ntlp | grep splunk >/dev/null && echo "[splunk has been already installed,if you want to reinstall it please press any key.]" && read -n 1
ps -ef | grep splunkd | grep -v grep | grep -v ./splunk | awk '{print $2}' | xargs kill -9
test -f /etc/init.d/splunk && rm -f /etc/init.d/splunk
test -d /usr/local/splunkforwarder && rm -rf /usr/local/splunkforwarder

#下载、安装、配置开机启动
cd ${DIRNAME}
tar -xzvf splunkforwarder-6.4.3-Linux.tgz

mv splunkforwarder /usr/local/
chown root:root -R /usr/local/splunkforwarder/

cd /usr/local/splunkforwarder/bin

./splunk start --accept-license

./splunk enable boot-start

#配置
#写入配置文件
cat >/usr/local/splunkforwarder/etc/system/local/deploymentclient.conf <<EOF
[target-broker:deploymentServer]
targetUri = ${Server}:8089
EOF

cat >/usr/local/splunkforwarder/etc/system/local/outputs.conf <<EOF
[tcpout]
defaultGroup = default-autolb-group

[tcpout:default-autolb-group]
server = ${Server}:9997

[tcpout-server://${Server}:9997]
EOF

cat >>/etc/rsyslog.conf <<EOF
*.* @${Server}:516
EOF

#检查安装结果
netstat -ntlp | grep splunk && echo -e "\033[33minstall Succeed.\033[0m" || echo -e "\033[31minstall Failed.\033[0m"

echo "                                  "
echo "###################################"
echo localhost ip address is ${IP}
echo "###################################"
