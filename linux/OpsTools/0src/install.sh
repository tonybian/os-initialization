#!/bin/bash

DIRNAME=$(
    cd $(dirname $0)
    pwd
)

. ${DIRNAME}/install.conf

LOG_FILE=${DIRNAME}/install.log

echo "----zabbix-agent install step----" >${LOG_FILE} 2>&1
sh ${DIRNAME}/zabbix/install.sh >>${DIRNAME}/install.log 2>&1

echo "----salt-minion install step----" >>${LOG_FILE}
sh ${DIRNAME}/salt-minion/install.sh >>${LOG_FILE} 2>&1

# echo "----TitanAgent install step----" >>${LOG_FILE}
# curl -s -L 'https://console.qingteng.cn/agent/download?k=be3c2e7d13518199656c38d53c9724589a64c46a&group=1005' | bash >>${LOG_FILE} 2>&1

# echo "----splunkforwarder install step----" >>${LOG_FILE}
# sh ${DIRNAME}/splunk/install.sh >>${LOG_FILE} 2>&1
