#!/bin/bash

DIRNAME=$(
    cd $(dirname $0)
    pwd
)
BASE_DIRNAME=$(dirname "$DIRNAME")

. $BASE_DIRNAME/install.conf

cat >/etc/yum.repos.d/saltstack.repo <<EOF
[saltstack-repo]
name=SaltStack repo for Red Hat Enterprise Linux \$releasever
baseurl=https://repo.saltstack.com/yum/redhat/\$releasever/\$basearch/latest
enabled=1
gpgcheck=1
gpgkey=https://repo.saltstack.com/yum/redhat/\$releasever/\$basearch/latest/SALTSTACK-GPG-KEY.pub
       https://repo.saltstack.com/yum/redhat/\$releasever/\$basearch/latest/base/RPM-GPG-KEY-CentOS-7
EOF

for i in $(rpm -qa | grep salt* 2>/dev/null); do
    rpm -e $i
done

yum -y install epel-release
yum -y install salt-minion

cat >/etc/salt/minion.d/id.conf <<EOF
id: $HOSTNAME
EOF

cat >/etc/salt/minion.d/master.conf <<EOF
master: $SALT_MASTER
EOF

cat >/etc/salt/minion.d/log_level.conf <<EOF
log_level: warning
EOF

cat >/etc/logrotate.d/salt-minion <<EOF
/var/log/salt/minion {
daily
missingok
rotate 7
nocompress
#delaycompress
notifempty
dateext
}
EOF

systemctl start salt-minion.service && systemctl enable salt-minion.service
