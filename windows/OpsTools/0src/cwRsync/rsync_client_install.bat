@Echo off

call ..\config.bat

call cwRsync_4.1.0_Installer.exe /S

copy /Y chmod.exe "C:\Program Files (x86)\cwRsync\bin"
copy /Y chown.exe "C:\Program Files (x86)\cwRsync\bin"

@Echo on