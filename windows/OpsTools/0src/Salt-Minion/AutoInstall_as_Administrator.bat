:: 一键安装salt-minion，理论支持所有windows系统
:: 原创：tongheb@tujia.com
:: 执行本脚本，自动安装salt-monion到C:\salt

@Echo off

call ..\config.bat

for /f "tokens=3 delims= " %%i in ('"reg query HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters /v Domain"') do (
set DOMAIN=%%i
)

call Salt-Minion-2017.7.2-Py2-x86-Setup.exe /S /master=%salt_master% /minion-name=%COMPUTERNAME%.%DOMAIN%

setlocal disabledelayedexpansion

xcopy /Y minion.d\_log_level.conf c:\salt\conf\minion.d\

set GrainConf=C:\salt\conf\minion.d\grains.conf

(
echo grains:
echo   admin:
echo     %USERNAME%
)> %GrainConf%

rem call c:
rem call cd c:\salt\conf
rem set "old=#log_level: warning"
rem set "new=log_level: critical"
rem (for /f "delims=" %%a in ('findstr /n .* minion.bak') do (
rem   set var=%%a
rem   setlocal enabledelayedexpansion
rem   if "!var:*:=!"=="" (echo;) else (
rem     set var=!var:*:=!
rem     echo;!var:%old%=%new%!
rem   )
rem   endlocal
rem )>>"minion"
rem )

rem set GrainConf=C:\salt\conf\minion.d\grain.conf
rem for /f "tokens=3 delims= " %%i in ('"reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Virtual Machine\Guest\Parameters" /v VirtualMachineId"') do (
rem set VirtualMachineId=%%i
rem )
rem @echo off
rem (
rem echo grains:
rem echo   VirtualMachineId:
rem echo     %VirtualMachineId%
rem )> %GrainConf%

echo "###salt-minion install###"
echo "Hostname=%COMPUTERNAME%.%DOMAIN%"

@Echo on


