@echo off
set parm1=%1
set parm2=%2

if defined parm2 (
@echo off
) else (
set parm2=%parm1%
)

netstat -ano | find "%parm1%" | find "%parm2%" | find /C " "