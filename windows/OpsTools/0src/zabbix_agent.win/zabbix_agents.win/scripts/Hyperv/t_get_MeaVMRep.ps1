$MeaVMRep =Measure-VMReplication
$GetVMRep =Get-VMReplication
$idx = 1

write-host "{"
write-host " `"data`":[`n"
foreach ($i in $MeaVMRep)
{
    write-host "  {"
    $line= "    `"{#RepName}`" : `"" + $i.Name + "`","
    write-host $line
    $line= "    `"{#RepState}`" : `"" + $i.State + "`","
    write-host $line
    $line= "    `"{#RepHealth}`" : `"" + $i.Health + "`","
    write-host $line
    $line= "    `"{#LRepTime}`" : `"" + $i.LReplTime + "`","
    write-host $line
    $line= "    `"{#RepAvgLatency}`" : `"" + $i.AvgLatency + "`""
    write-host $line   
    write-host "  }"
    
    if ($idx -lt $MeaVMRep.Count)
        {
            write-host "  ,"
        }
        
    $idx++;
}

write-host
write-host " ]"
write-host "}"