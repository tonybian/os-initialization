$MeaVMRep =Measure-VMReplication
$idx = 1

write-host "{"
write-host " `"data`":[`n"
foreach ($i in $MeaVMRep)
{
    if ($idx -lt $MeaVMRep.Count)
    {
        $line= "   { `"{#REPNAME}`" : `"" + $i.Name + "`" },"
        write-host $line    
    }
    else
    {
        $line= "   { `"{#REPNAME}`" : `"" + $i.Name + "`" }"
        write-host $line 
    }        
    $idx++;
}

write-host
write-host " ]"
write-host "}"