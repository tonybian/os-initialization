$MeaVMRep = Measure-VMReplication
$GetVMRep = Get-VMReplication

$idx = 1
write-host "{"
write-host "  `"data`":[`n"

foreach ($i in $MeaVMRep)
{
    foreach ($j in $GetVMRep)
    {
        if ( $i.name -eq $j.name )
        {
            write-host "  {"
            $line= "      `"{#RepName}`" : `"" + $i.Name + "`","
            write-host $line
            $line= "      `"{#RepState}`" : `"" + $i.State + "`","
            write-host $line
            $line= "      `"{#RepHealth}`" : `"" + $i.Health + "`","
            write-host $line
            $line= "      `"{#RepLReplTime}`" : `"" + $i.LReplTime + "`","
            write-host $line
            $line= "      `"{#RepPReplSize}`" : `"" + $i.PReplSize + "`","
            write-host $line
            $line= "      `"{#RepAvgLatency}`" : `"" + $i.AvgLatency + "`","
            write-host $line
            $line= "      `"{#RepAvgReplSize}`" : `"" + $i.AvgReplSize + "`","
            write-host $line
            
            $line= "      `"{#RepMode}`" : `"" + $j.Mode + "`","
            write-host $line
            $line= "      `"{#RepFrequencySec}`" : `"" + $j.FrequencySec + "`","
            write-host $line
            $line= "      `"{#RepPrimaryServer}`" : `"" + $j.PrimaryServer + "`","
            write-host $line
            $line= "      `"{#RepReplicaServer}`" : `"" + $j.ReplicaServer + "`""
            write-host $line
            $line= "      `"{#RepReplicaPort}`" : `"" + $j.ReplicaPort + "`""
            write-host $line
            $line= "      `"{#RepAuthType}`" : `"" + $j.AuthType + "`""
            write-host $line
            write-host "  }"
            
            $idx++;
            
            if ($idx -le $GetVMRep.Count)
            {
                write-host "  ,"
            }
        }         
    }    
}

write-host
write-host "  ]"
write-host "}"
