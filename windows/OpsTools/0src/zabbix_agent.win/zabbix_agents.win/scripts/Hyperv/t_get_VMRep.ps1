$GetVMRep = Get-VMReplication
$MeaVMRep = Measure-VMReplication
$idx = 1
write-host "{"
write-host "  `"data`":[`n"
foreach($i in $GetVMRep)
{
    $j=$MeaVMRep|?{$_.name -eq $i.name}
    $i|add-member -membertype noteproperty -name LReplTime -value ($j.LReplTime) -Force
    $i|add-member -membertype noteproperty -name AvgLatency -value ($j.AvgLatency) -Force
    $i|add-member -membertype noteproperty -name PReplSize -value ($j.PReplSize) -Force
    $i|add-member -membertype noteproperty -name AvgReplSize -value ($j.AvgReplSize) -Force
    write-host "  {"
    $line= "    `"{#RepName}`" : `"" + $i.Name + "`","
    write-host $line
    $line= "    `"{#RepState}`" : `"" + $i.State + "`","
    write-host $line
    $line= "    `"{#RepHealth}`" : `"" + $i.Health + "`","
    write-host $line
    $line= "    `"{#RepLReplTime}`" : `"" + $i.LReplTime + "`","
    write-host $line
    $line= "    `"{#RepPReplSize}`" : `"" + $i.PReplSize + "`","
    write-host $line
    $line= "    `"{#RepAvgLatency}`" : `"" + $i.AvgLatency + "`","
    write-host $line
    $line= "    `"{#RepAvgReplSize}`" : `"" + $i.AvgReplSize + "`","
    write-host $line
    $line= "    `"{#RepMode}`" : `"" + $i.Mode + "`","
    write-host $line
    $line= "    `"{#RepFrequencySec}`" : `"" + $i.FrequencySec + "`","
    write-host $line
    $line= "    `"{#RepPrimaryServer}`" : `"" + $i.PrimaryServer + "`","
    write-host $line
    $line= "    `"{#RepReplicaServer}`" : `"" + $i.ReplicaServer + "`","
    write-host $line
    $line= "    `"{#RepReplicaPort}`" : `"" + $i.ReplicaPort + "`","
    write-host $line
    $line= "    `"{#RepAuthType}`" : `"" + $i.AuthType + "`""
    write-host $line
    write-host "  }"
    $idx++;
    if ($idx -le $GetVMRep.Count)
    {
        write-host "  ,"
    }
}
write-host
write-host "  ]"
write-host "}"