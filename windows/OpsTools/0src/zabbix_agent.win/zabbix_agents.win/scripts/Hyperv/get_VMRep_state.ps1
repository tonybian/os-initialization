param ([string] $Name = 0, $Var = 0)

if (($Var -eq "LReplTime") -or ($Var -eq "PReplSize") -or ($Var -eq "AvgReplSize"))
{
    Write-Output ((Measure-VMReplication |? {$_.name -eq "$Name"}).$Var)
}
elseif ($Var -eq "AvgLatency")
{
    Write-Output ((Measure-VMReplication |? {$_.name -eq "$Name"}).$Var.Seconds)
}
else
{
    Write-Output ((Get-VMReplication |? {$_.name -eq "$Name"}).$Var)
}