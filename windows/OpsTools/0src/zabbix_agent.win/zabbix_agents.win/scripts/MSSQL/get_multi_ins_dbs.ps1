$defdbs = Get-WmiObject Win32_PerfFormattedData_MSSQLServer_SQLServerDatabases | Select Name
$INSTANCES = Get-WmiObject Win32_Service| Where-Object {$_.name -like "MSSQL$*"} | select Name

$idx = 1
write-host "{"
write-host " `"data`":[`n"

foreach ($i in $defdbs)
{
    write-host "  {"
    $line= "    `"{#INSTANCE}`" : `"" + "SQLServer" + "`" ,"
    write-host $line
    $line= "    `"{#DBS}`" : `"" + $i.Name + "`" "
    write-host $line
    write-host "  }"
    write-host "  ,"

    $idx++;
}

$idx = 1
foreach ($i in $INSTANCES)
{
    $x = $i.Name.SubString(6)
    $dbs = Get-WmiObject Win32_PerfFormattedData_MSSQL$x`_MSSQL$x`Databases | select name
    
    $idx_inner = 1
    foreach ($j in $dbs)
    {
        write-host "  {"
        $line= "    `"{#INSTANCE}`" : `"" + $i.Name + "`" ,"
        write-host $line
        $line= "    `"{#DBS}`" : `"" + $j.Name + "`" "
        write-host $line
        write-host "  }"
        write-host "  ,"
        
        $idx_inner++;
    }
    $idx++;
}

$line= "  { `"{#END}`" : `"" + "END" + "`" }"
write-host $line

write-host
write-host " ]"
write-host "}"