set nocount on

SELECT t3.host_name, t3.program_name, t3.login_name, t2.sql_handle,st.text
  from sys.dm_db_session_space_usage as t1, sys.dm_exec_sessions as t3, sys.dm_exec_requests as t2
 CROSS APPLY sys.dm_exec_sql_text(t2.sql_handle) AS st
 where t1.session_id = t2.session_id
   and t1.session_id = t3.session_id
   and t1.session_id > 50
   and (t1.internal_objects_alloc_page_count > 0 or
       t1.user_objects_alloc_page_count > 0 or
       t1.internal_objects_dealloc_page_count > 0 or
       t1.user_objects_dealloc_page_count > 0);