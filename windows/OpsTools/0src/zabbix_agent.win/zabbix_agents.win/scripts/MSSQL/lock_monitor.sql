set nocount on

CREATE Table tempdb.#Who(spid int,
    ecid int,
    status nvarchar(20),
    loginname nvarchar(20),
    hostname nvarchar(20),
    blk int,
    dbname nvarchar(20),
    cmd nvarchar(50),
    request_ID int);

CREATE Table tempdb.#Lock(spid int,
    dpid int,
    objid int,
    indld int,
    [Type] nvarchar(20),
    Resource nvarchar(50),
    Mode nvarchar(10),
    Status nvarchar(10)
);

INSERT INTO tempdb.#Who
    EXEC sp_who active  --看哪个引起的阻塞，blk 
INSERT INTO tempdb.#Lock
    EXEC sp_lock  --看锁住了那个资源id，objid 

--最后发送到SQL Server的语句
DECLARE crsr Cursor FOR
    SELECT spid FROM tempdb.#Who WHERE blk != 0;
DECLARE @spid int;
open crsr;
FETCH NEXT FROM crsr INTO @spid;
WHILE (@@FETCH_STATUS = 0)
BEGIN;
    dbcc inputbuffer(@spid);
    FETCH NEXT FROM crsr INTO @spid;
END;
close crsr;
DEALLOCATE crsr;

--锁定的资源
SELECT tempdb.#Who.spid,
       hostname,
       dbname,
       objid,
       type,
       mode,
       object_name(objid) as objName
  FROM tempdb.#Lock
  JOIN tempdb.#Who
    ON tempdb.#Who.spid = tempdb.#Lock.spid
 WHERE objid != 0
   and tempdb.#Who.blk != 0;

DROP Table tempdb.#Who;
DROP Table tempdb.#Lock;