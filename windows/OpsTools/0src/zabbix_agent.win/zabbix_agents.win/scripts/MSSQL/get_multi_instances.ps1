$INSTANCES = Get-WmiObject Win32_Service| Where-Object {$_.name -like "MSSQL$*"} | select Name 
$DEFINSTANCE = Get-WmiObject Win32_Service| Where-Object {$_.name -like "MSSQLSERVER"} | select Name
write-host "{"
write-host " `"data`":[`n"
if ($DEFINSTANCE.Name -eq "MSSQLSERVER")
{
    if ($INSTANCES.Name)
    {
    $line= "{ `"{#DEFINSTANCE}`" : `"" + "MSSQLSERVER" + "`" },"
    write-host $line
    }
    else
    {
    $line= "{ `"{#DEFINSTANCE}`" : `"" + "MSSQLSERVER" + "`" }"
    write-host $line
    }
}
$idx = 1
foreach ($i in $INSTANCES)
{
    $line= "{ `"{#INSTANCES}`" : `"" + $i.Name + "`" },"
    write-host $line

    $x = $i.Name.SubString(6)
    if ($idx -lt $INSTANCES.Count)
    {
        $line= "{ `"{#INS}`" : `"" + $x + "`" },"
        write-host $line
    }
    elseif ($idx -ge $INSTANCES.Count)
    {
        $line= "{ `"{#INS}`" : `"" + $x + "`" }"
        write-host $line
    }
    $idx++;
}
write-host
write-host " ]"
write-host "}"
