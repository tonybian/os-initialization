:: 一键安装zabbix agent，理论支持所有windows系统
:: 原创作者：qicheng0211.blog.51cto.com
:: 修改：tongheb@tujia.com
:: 执行本脚本，自动安装zabbix agent到%ParentDirectory%

@Echo off

call ..\config.bat

:: 需要修改hostname为本地hostname
:: set /p hostname=请输入本机HOSTNAME（不要包含域名）: 

:: 重要变量，不可修改
@set script_base=%~dp0
@set Directory=%script_base:~0,-1%

for %%d in (%Directory%) do set ParentDirectory_1=%%~dpd

@set ParentDirectory_1=%ParentDirectory_1:~0,-1%

for %%d in (%ParentDirectory_1%) do set ParentDirectory=%%~dpd

set logfile=%ParentDirectory%zabbix_agent\zabbix_agentd.log
set conf_file=%~dp0\zabbix_agents.win\conf\zabbix_agentd.win.conf
copy "%conf_file%" "%conf_file%"_bak.txt

echo LogFile=%logfile% > "%conf_file%"
echo LogFileSize=10 >> "%conf_file%"
echo Include=%ParentDirectory%zabbix_agent\zabbix_agentd.conf.d\*.conf >> "%conf_file%"
echo Timeout=30 >> "%conf_file%"
echo Server=%zabbix_server% >> "%conf_file%"
echo ServerActive=%zabbix_server% >> "%conf_file%"
echo Hostname=%computername% >> "%conf_file%"
echo EnableRemoteCommands=1 >> "%conf_file%"
echo LogRemoteCommands=1 >> "%conf_file%"
echo UnsafeUserParameters=1 >> "%conf_file%"

:: 替换配置文件中的server ip
:: set conf_file=%~dp0\zabbix_agents.win\conf\zabbix_agentd.win.conf
:: for /f "delims=" %%a in ('type "%conf_file%"') do (
::   set str=%%a
::   set "str=!str:127.0.0.1=%zabbix_server%!"
::   echo !str!>>"%conf_file%"_tmp.txt
:: )
:: move "%conf_file%" "%conf_file%"_bak.txt
:: move "%conf_file%"_tmp.txt "%conf_file%"

:: 32 bit or 64 bit process detection
IF "%PROCESSOR_ARCHITECTURE%%PROCESSOR_ARCHITEW6432%"=="x86" (
  set _processor_architecture=32bit
  goto x86
) ELSE (
  set _processor_architecture=64bit
  goto x64
)

:x86
xcopy "%~dp0\zabbix_agents.win\bin\win32" %ParentDirectory%zabbix_agent /e /i /y
goto install

:x64
xcopy "%~dp0\zabbix_agents.win\bin\win64" %ParentDirectory%zabbix_agent /e /i /y
goto install

:install
copy "%conf_file%" %ParentDirectory%zabbix_agent\zabbix_agentd.conf /y
xcopy "%~dp0\zabbix_agents.win\scripts" %ParentDirectory%zabbix_agent\scripts /e /i /y
xcopy "%~dp0\zabbix_agents.win\zabbix_agentd.conf.d" %ParentDirectory%zabbix_agent\zabbix_agentd.conf.d /e /i /y
sc stop  "Zabbix Agent" >nul 2>nul
sc delete  "Zabbix Agent" >nul 2>nul
%ParentDirectory%zabbix_agent\zabbix_agentd.exe -c %ParentDirectory%zabbix_agent\zabbix_agentd.conf -i
%ParentDirectory%zabbix_agent\zabbix_agentd.exe -c %ParentDirectory%zabbix_agent\zabbix_agentd.conf -s

::firewall
:: Get windows Version numbers
For /f "tokens=2 delims=[]" %%G in ('ver') Do (set _version=%%G) 
For /f "tokens=2,3,4 delims=. " %%G in ('echo %_version%') Do (set _major=%%G& set _minor=%%H& set _build=%%I) 
Echo Major version: %_major%  Minor Version: %_minor%.%_build%

:: OS detection
IF "%_major%"=="5" (
  IF "%_minor%"=="0" Echo OS details: Windows 2000 [%_processor_architecture%]
  IF "%_minor%"=="1" Echo OS details: Windows XP [%_processor_architecture%]
  IF "%_minor%"=="2" IF "%_processor_architecture%"=="32bit" Echo OS details: Windows 2003 [%_processor_architecture%]
  IF "%_minor%"=="2" IF "%_processor_architecture%"=="64bit" Echo OS details: Windows 2003 or XP 64 bit [%_processor_architecture%]
  :: 开启防火墙10050端口
  netsh firewall delete portopening protocol=tcp port=10050
  netsh firewall add portopening protocol=tcp port=10050 name=zabbix_10050 mode=enable scope=custom
) ELSE IF "%_major%"=="6" (
  IF "%_minor%"=="0" Echo OS details: Windows Vista or Windows 2008 [%_processor_architecture%]
  IF "%_minor%"=="1" Echo OS details: Windows 7 or Windows 2008 R2 [%_processor_architecture%]
  IF "%_minor%"=="2" Echo OS details: Windows 8 or Windows Server 2012 [%_processor_architecture%]
  IF "%_minor%"=="3" Echo OS details: Windows 8.1 or Windows Server 2012 R2 [%_processor_architecture%]
  IF "%_minor%"=="4" Echo OS details: Windows 10 Technical Preview [%_processor_architecture%]
  :: 开启防火墙10050端口
  netsh advfirewall firewall delete rule name="zabbix_agent_10050"
  netsh advfirewall firewall delete rule name="zabbix_10050"
  netsh advfirewall firewall add rule name="zabbix_agent_10050" protocol=TCP dir=in localport=10050 action=allow
)

::ScheduleJob

schtasks /create /F /tn ntp_update /tr %ParentDirectory%zabbix_agent\scripts\ntp_update.bat /sc DAILY /ri 60 /du 24:00 /ru "NT Authority\System"  2>&1

:: 服务启动检查

net start|findstr /i /c:"Zabbix Agent">nul&&goto :ks2

:ks1

echo ############################################################
echo #           安装失败，请联系Zabbix系统管理员。             #
echo ############################################################

goto :exit

:ks2 

echo ############################################################
echo #     安装成功，请向Zabbix系统管理员提供IP地址和主机名。   #
echo ############################################################

goto :exit

:exit

:: rd /s /q "%~dp0\zabbix_agents.win"
:: del %0

echo "###zabbix_agent install###"
echo "Hostname=%computername%"

@Echo on


