:: 一键安装salt-minion，理论支持所有windows系统
:: 原创：tongheb@tujia.com
:: 执行本脚本，自动安装salt-monion到C:\salt

@Echo off
setlocal enabledelayedexpansion

call config.bat

echo "----rsync_client install step----" > %install_base%install.log 2>&1
call cd %install_base%cwRsync
call rsync_client_install.bat >> %install_base%install.log 2>&1

echo "----salt-minion install step----" >> %install_base%install.log 2>&1
call cd %install_base%Salt-Minion
call AutoInstall_as_Administrator.bat >> %install_base%install.log 2>&1

echo "----zabbix_agentd install step----" >> %install_base%install.log 2>&1
call cd %install_base%zabbix_agent.win
call AutoInstall_as_Administrator.bat >> %install_base%install.log 2>&1

rem echo "----splunkforwarder install step----" >> %install_base%install.log 2>&1
rem call cd %install_base%splunk
rem call AutoInstall_as_Administrator.bat >> %install_base%install.log 2>&1
rem 
rem echo "----TitanAgent install step----" >> %install_base%install.log 2>&1
rem call cd %install_base%TitanAgent
rem call AutoInstall.bat >> %install_base%install.log 2>&1

@Echo on
