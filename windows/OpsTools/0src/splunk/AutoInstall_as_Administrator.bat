@Echo off

call ..\config.bat

call config.bat -o A -p 3 -v

msiexec /i splunkforwarder-6.4.3-windows.msi /qn+ /l*v msiexec_log1.txt UILicenseAgreement="1" AGREETOLICENSE="Yes" UICustomize="2" UIDeplSrv="%splunk_server%" UIDeplSrvPort="8089" UIRecvIdx="%splunk_server%" UIRecvIdxPort="9997" ProductName="UniversalForwarder" RECEIVING_INDEXER="%splunk_server%:9997" DEPLOYMENT_SERVER="%splunk_server%:8089" WINEVENTLOG_APP_ENABLE="1" WINEVENTLOG_SEC_ENABLE="1" WINEVENTLOG_SYS_ENABLE="1" ADDLOCAL="Complete,WixSwidTag"

@Echo on