@echo off
if {%1} == {} goto :help
if {%2} == {} goto :help

if exist SAMTool.sdb erase SAMTool.sdb /q
if exist SAMTool.inf erase SAMTool.inf /q
if exist SAMTool.log erase SAMTool.log /q

if {%1} == {-b} secedit /export /cfg %2 /log SAMTool.log /quiet

if {%1} == {-r} secedit /configure /db SAMTool.sdb /cfg %2 /log SAMTool.log /quiet

if {%1} == {-o} (
 if {%4} == {}       goto :help
 if not {%3} == {-p} goto :help

 echo %4 | findstr "[0-3]" >nul || goto :help

 rem pushd %windir%\system32\
 echo.[version]       >>SAMTool.inf
 echo.signature="$CHICAGO$"    >>SAMTool.inf
 echo.[Event Audit]      >>SAMTool.inf

 echo.%2 | findstr "D" >nul && echo.AuditDSAccess=%4   >>SAMTool.inf
 echo.%2 | findstr "E" >nul && echo.AuditLogonEvents=%4  >>SAMTool.inf
 echo.%2 | findstr "S" >nul && echo.AuditSystemEvents=%4  >>SAMTool.inf
 echo.%2 | findstr "O" >nul && echo.AuditObjectAccess=%4  >>SAMTool.inf
 echo.%2 | findstr "U" >nul && echo.AuditPrivilegeUse=%4  >>SAMTool.inf
 echo.%2 | findstr "C" >nul && echo.AuditPolicyChange=%4  >>SAMTool.inf
 echo.%2 | findstr "L" >nul && echo.AuditAccountLogon=%4  >>SAMTool.inf
 echo.%2 | findstr "M" >nul && echo.AuditAccountManage=%4  >>SAMTool.inf
 echo.%2 | findstr "P" >nul && echo.AuditProcessTracking=%4 >>SAMTool.inf

 if {%2} == {A} (
  echo.AuditDSAccess=%4     >>SAMTool.inf
  echo.AuditLogonEvents=%4    >>SAMTool.inf
  echo.AuditSystemEvents=%4    >>SAMTool.inf
  echo.AuditObjectAccess=%4    >>SAMTool.inf
  echo.AuditPrivilegeUse=%4    >>SAMTool.inf
  echo.AuditPolicyChange=%4    >>SAMTool.inf
  echo.AuditAccountLogon=%4    >>SAMTool.inf
  echo.AuditAccountManage=%4    >>SAMTool.inf
  echo.AuditProcessTracking=%4   >>SAMTool.inf
 )
 secedit /configure /db SAMTool.sdb /cfg SAMTool.inf /log SAMTool.log /quiet
)

if {%3} == {-v} type SAMTool.log
if {%5} == {-v} type SAMTool.log

if exist SAMTool.sdb erase SAMTool.sdb /q
if exist SAMTool.inf erase SAMTool.inf /q
if exist SAMTool.log erase SAMTool.log /q

exit /b

:help
cls
echo.System audit strategy manage tool. (C) Copyright 2013 enun-net.
echo.
echo.Usage: SAMTool -b^|r [drive:][path][filename] -o options -p parameters -v
echo.        
echo.         -b Backup the current configuration, Specifies an INF file.
echo.         -r From an INF file recovery configuration.
echo.         -o options^(Support multiple^):
echo.     D: Directory Service Access
echo.     E: Logon Events
echo.     S: System Events
echo.     O: Object Access
echo.     U: Privilege Use
echo.     C: Policy Change
echo.     L: Account Logon
echo.     M: Account Manage
echo.     P: Process Tracking
echo.     A: All audit
echo.         -p parameters:
echo.     0: Don't audit
echo.     1: Only audit successful
echo.     2: Only audit failure
echo.     3: All audit ^(successful and failure^)
echo.         -v Detailed results.
echo.
echo.Example: SAMTool -o EC -p 0 -v
echo.         SAMTool -b c:\myconfig.inf -v
exit /b

@Echo on