@Echo off
rem setlocal enabledelayedexpansion

:: 检验权限
set TempFile_Name=%SystemRoot%\System32\BatTestUACin_SysRt%Random%.batemp
:: echo %TempFile_Name%
 
( echo "BAT Test UAC in Temp" >%TempFile_Name% ) 1>nul 2>nul

:: 判断权限
if exist %TempFile_Name% (
goto install
) else (
echo ############################################################
echo #         权限不足，请使用本地管理员身份运行此脚本         #
echo ############################################################
echo 请按任意键退出
pause >nul
exit
)

:install 

@set install_base=%~dp0
@set salt_master=salt.corp.lc.com
@set zabbix_server=zabbix.corp.lancare.com
@set splunk_server=splunk.corp.lancare.com

:exit
@Echo on
