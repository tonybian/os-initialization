@echo off 

set /a StartMS=%time:~3,1%*600000 + %time:~4,1%*60000 + %time:~6,1%*10000 + %time:~7,1%*1000 + %time:~9,1%*100 + %time:~10,1%*10 

%1 %2 %3 %4 %5 %6 

set /a EndMS =%time:~3,1%*600000 + %time:~4,1%*60000 + %time:~6,1%*10000 + %time:~7,1%*1000 + %time:~9,1%*100 + %time:~10,1%*10

set /a realtime = %EndMS%-%StartMS% 

echo %realtime%ms 
